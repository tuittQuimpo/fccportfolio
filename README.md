# FCC Portfolio

## Objectives

- Create an HTML Project in Gitlab
- Build the Skeleton of the Web Developer Portfolio
- Add Styling to the Portfolio
- Deploy the LIVE Web Developer Portfolio in Gitlab Pages

### Create an HTML project in Gitlab

We will create an HTML in Gitlab and clone it to our local machine

#### Creating the Project in Gitlab

1. Login to your Gitlab account
1. In your Projects Timeline/Dashboard, click on New Project
1. Select Create From Template
1. Select Pages/Plain HTML and click on Use template
1. Put fccportfolio as the Project Name
1. Change the Visibility level from private to public
1. Next step is to create a copy of this project to our local machines. Do not close your CLI.

#### Cloning the Project Locally

Cloning means "creating a copy" or downloading the project so we can build the HTML project in our local machines/computers

1. In your machines, go to your Documents folder and right click on it

- For Windows users, select Gitbash Here from the options
- For Mac users, select Open Terminal Here or drag the Documents folder to the Terminal icon in the Dock
- For Linux users, select Open Terminal

1. Writing the pwd(present working directory) command in the Command Line should return a value of the location you are currently in. (Instructor to show his pwd). Our desired output should show that we are inside the Documents folder.
1. Let's set up our Gitlab user credentials to our machines.

- Execute the command git config --global user.name "Full Name in Gitlab"
- Check if we inputted the correct value by running the command git config --global user.name
- Execute the command git config --global user.email "Email in Gitlab"
- Check if we inputted the correct value by running the command git config --global user.email

These steps will enable our Gitbash/Terminal know who is currently logged in and will enable us to communicate with Gitlab

1. Since our local machine is ready, let's go back to our fccportfolio project/repository in Gitlab. (Open your browser and show the fccportfolio repo tab)
1. Click on the clone button and select clone with https. SSH approach is okay but the instructor should go through the steps to creating an ssh key.
1. Copy the value inside the Clone with HTTPS input box
1. Switch to your CLI and run the command git clone then paste the URL from Gitlab

- git clone https://gitlab.com/johndoe/fccportfolio.git

1. Input your credentials and press enter. Wait for it to download/clone

- Reminders:
  - Pressing the up arrow key inside the CLI will return the last command you executed
  - While inputting your password, no keys will appear but you are actually already typing your password

1. Check if the fccportfolio was cloned to the local machine

### Build the Skeleton of the Web Developer Portfolio

The HTML document is considered the building blocks/foundation of websites. All styling will be placed inside a separate stylesheet called a CSS file. We will use the text editor called Sublime Text to write our code.

- Open your Sublime Text Application
- Select on File
- Select on Open Folder
- Select the fccportfolio folder inside Desktop and click Open Folder
- fccfolder showed now appear at the left side nav of the Sublime Text application
- Double click on the index.html file inside the public folder
- Go inside the index.html tab and change the value of the title to "John Doe | Web Developer Portfolio"

```html
<title>John Doe | Web Developer Portfolio</title>
```

- Right click anywhere inside the file and select open in browser. Show that the value inputted in the title is seen in the browser tab.
- The output that we see in our browser are the elements inside the HTML element called body. Erase everything inside the body. Save and refresh the browser to show that the page is blank.

* Inside <body>, create a div element with the class container that will serve as the wrapper of our whole layout.

```html
<!-- creates a wrapper/container for our layout -->
<div class="container"></div>
```

- inside the div, add 2 <section> elements. One with an id called landing and the other one gallery

```html
<!-- creates a wrapper/container for our layout -->
<div class="container">
	<section id="landing"></section>
	<!-- end landing -->

	<section id="gallery"></section>
	<!-- end gallery -->
</div>
```

- inside #landing, add an h1 element with the full name of the dev and an h2 element with Full Stack Web Developer as its value. Save and check the output

```html
<section id="landing">
	<h1>John Doe</h1>
	<h2>Full Stack Web Developer</h2>
</section>
```

- let's add a profile picture. inisde the public folder, create a folder called images.
- Download an image of yourself and place it inside your public/images folder

```html
<section id="landing">
	<h1>Full Stack Web Developer</h1>
	<p>Hi! My name is John and I love building things from scratch.</p>
	<img src="images/mf-avatar.svg" />
</section>
```

- Inside #gallery, add two h2 elements with "Languages I Speak" and "Tools I Use" as its content

```html
<section id="gallery">
	<h2>Languages I Speak</h2>
	<h2>Tools I Use</h2>
</section>
```

- Under Languages I Speak, create an unordered list and list the following items:

```html
<section id="gallery">
	<h2>Languages I Speak</h2>
	<ul>
		<li>HTML</li>
		<li>CSS</li>
	</ul>
	<h2>Tools I Use</h2>
</section>
```

- Under Tools I Use, create an unordered list and list the following items:

```html
<section id="gallery">
	<h2>Languages I Speak</h2>
	<ul>
		<li>HTML</li>
		<li>CSS</li>
	</ul>
	<h2>Tools I Use</h2>
	<ul>
		<li>Terminal</li>
		<li>Gitbash</li>
		<li>Sublime Text</li>
		<li>Gitlab</li>
	</ul>
</section>
```

- now let's add our styling

### Add Styling to the Portfolio

After creating the skeleton, we will now add styling to our webpage

#### Choose the font style/s that we will use

- Go to fonts.google.com
- Select Montserrat Semi-Bold 600 and Roboto Regular 400
- Copy the link and place it inisde <head>

```html
<!-- fonts -->
<link rel="preconnect" href="https://fonts.gstatic.com" />
<link
	href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&family=Roboto&display=swap"
	rel="stylesheet"
/>
```

- open up your style.css file, and remove everything inside it
- Let's declare a default font style. The universal(\*) selector selects all HTML elements. Targetting it will enable us to declare a font style for all html elements. Declaring a default font enables us to control how our text will look and not rely with what the browser gives us.

```css
* {
	font-family: "Roboto", sans-serif;
}
```

- Next, let's select our heading elements, h1 and h2 and declare Montserrat as the font-style for the said elements

```css
h1,
h2 {
	font-family: "Montserrat", sans-serif;
}
```

#### Bootstrap

For clients and companies, time to market is as important as the tech stack you used to create a certain project. Bootstrap will enable us to develop mobile responsive layouts faster compared to using plain css.

- go to https://getbootstrap.com/
- go to docs and select v4.5x as this is currently the more stable version compared to Bootstrap 5.
- Scroll down to the starter template version and copy the required meta tags and link to the CSS library of Bootstrap
- Paste it inside the head element of your project

  - Paste the Bootstrap CSS link before the our own CSS file

  ```html
  <head>
  	<!-- Required meta tags -->
  	<meta charset="utf-8" />
  	<meta name="viewport" content="width=device-width, initial-scale=1" />
  	<meta name="generator" content="GitLab Pages" />
  	<title>John Doe | Web Developer Portfolio</title>

  	<!-- fonts -->
  	<link rel="preconnect" href="https://fonts.gstatic.com" />
  	<link
  		href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&family=Roboto&display=swap"
  		rel="stylesheet"
  	/>

  	<!-- Bootstrap CSS -->
  	<link
  		rel="stylesheet"
  		href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
  		integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
  		crossorigin="anonymous"
  	/>

  	<!-- external css -->
  	<link rel="stylesheet" href="style.css" />
  </head>
  ```

- Copy the Bootstrap script dependencies and place it before the closing body tag

```html
	<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

	</body>
```

- Did you notice the space that was generated after linking the Bootstrap dependencies? This is because of the class container that we used for our div element. By default, Bootstrap has 2 types of wrappers/containers that we can use so we can control the layout of our webpage. Class container sets a max width, hence the space at the sides. Class container-fluid creates a fluid layout which will make our layout span the whole width of the screen. Let's change the class container to container-fluid to achieve the fluid layout

```html
<!-- creates a wrapper/container for our layout -->
<div class="container-fluid">existing code inside</div>
```

- next step is to create a mobile responsive navbar. Before the .container-fluid and let's add a navbar.
- go to the components page and click on navbar. copy the first example and paste in your html file
- Save and refresh the page.
- After refreshing, let's clean up the navbar
- Change .navbar-light to .navbar-dark so the text will be a light color instead of a dark one.
- Change .bg-light to .bg-primary to change the background color of the navbar to blue.
- Remove the anchor element with .navbar-brand
- Remove the aria attributes
- Change the value of the data-target to #portfolio-nav
- For the div element with .navbar-collapse, change its ID with the same value as the one indicated for data-target
- Change .mr-auto to .mx-auto to center the list of nav items to the right most of the navbar
- Delete the list items inside the unordered list, and let's create our own.
- When a user clicks on Home, user will be redirected to the element with the ID, landing
- When a user clicks on Gallery, user will be redirected to the element with the ID, gallery
- Show the effect when on mobile

```html
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	<button
		class="navbar-toggler"
		type="button"
		data-toggle="collapse"
		data-target="#portfolio-nav"
	>
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="portfolio-nav">
		<ul class="navbar-nav mx-auto">
			<li class="nav-item">
				<a href="#landing" class="nav-link"> Home </a>
			</li>
			<li class="nav-item">
				<a href="#gallery" class="nav-link"> Gallery </a>
			</li>
		</ul>
	</div>
</nav>
```

#### Adding Bootstrap styling to our Page

- Add the class jumbotron to #landing. This will enable us to use Bootstrap's jumbotron component which is perfect for landing pages/sections.

```html
<section class="jumbotron" id="landing">
	<h1>Full Stack Web Developer</h1>
	<p>Hi! My name is John and I love building things from scratch.</p>
	<img src="images/mf-avatar.svg" />
</section>
<!-- end landing -->
```

- Add the class text center which centers the text horizontally
- Add the class img-fluid to the image to avoid the image from overflowing
- Add the class mt-3 to add a top margin to our element

```html
<section class="jumbotron text-center mt-3" id="landing">
	<h1>Full Stack Web Developer</h1>
	<p>Hi! My name is John and I love building things from scratch.</p>
	<img src="images/mf-avatar.svg" class="img-fluid" />
</section>
<!-- end landing -->
```

- For #gallery, add the class text-center to center the text horizontally

```html
<section id="gallery" class="text-center">
	<h2>Languages I Speak</h2>
	<ul>
		<li>HTML</li>
		<li>CSS</li>
	</ul>
	<h2>Tools I Use</h2>
	<ul>
		<li>Terminal</li>
		<li>Gitbash</li>
		<li>Sublime Text</li>
		<li>Gitlab</li>
	</ul>
</section>
<!-- end gallery -->
```

#### Add styling via external CSS

- Center the elements inside landing using flexbox

```css
#landing {
	height: 80vh;
	/* center the content vertically using flexbox */
	display: flex;
	flex-direction: column;
	justify-content: center;
}
```

- Optional: if we want to remove the background color of .jumbotron

```css
.jumbotron {
	background-color: transparent;
}
```

- Target the lists inside #gallery and stack them side by side

### Deploy the LIVE Web Developer Portfolio in Gitlab Pages
